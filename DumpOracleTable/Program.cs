﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using DumpOracleTable.CommandLineParameters;
using DumpOracleTable.Security;
using DumpOracleTable.Settings;
using Microsoft.Extensions.Configuration;


namespace DumpOracleTable
{
    class Program
    {
        static int Main(string[] args)
        {
            /*
             * Configure appsettings.json usage with code below
             * Bind a section of appsettings to a predefined class, _appsettings
             */
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .AddEnvironmentVariables();

            IConfigurationRoot configuration = builder.Build();
            var _appSettings = new AppSettings();
            configuration.GetSection("AppSettings").Bind(_appSettings);
                
            
            /*
             * Validate arguments passed.
             */
            CommandLineArguments _args = new CommandLineArguments();
            if (!_args.ValidArguments)
            {
                Console.WriteLine(_args);
                return 1;
            }
            
            /*
             * Verify database connectivity
             */
            var _instance = OracleDatabase.Instance;

            _instance.UserName = _appSettings.Username;
            _instance.Password = OraclePasswordManager.Instance.GetOraclePassword(_appSettings.Database);
            _instance.DataSource = configuration.GetConnectionString(String.IsNullOrEmpty(_args["db"])? _appSettings.Database:_args["db"]);
            _instance.DateFormat = _args.DateFormat;
            _instance.RowLimit = _args.RowLimit;
            
            if (_instance.Available())
            {
                Console.WriteLine(_instance);
                Console.WriteLine($"Connected to database {_args["db"]}");
            }
            else
            {
                Console.WriteLine(_instance);
                Console.WriteLine("Unable to connect to database");
                return 3;
            }
            
            bool _result = _instance.GetDumpDetails(_args.Tablename);
            if  ( (_result) )
            {
                Console.WriteLine($"Writing contents of table {_instance.TableName}");
            }
            else
            {
                Console.WriteLine($"Unable to get export details for table {_args["table"]}");
                return 2;
            }

            OracleTableFileWriter writer = new OracleTableFileWriter()
            {
                FileNumber = _instance.FileNumber,
                Delimiter = _instance.Delimiter,
                SequenceNo = _instance.SequenceNo,
                HasHeader = true,
                HasFooter = true,
                ChunkSize = _args.FeedbackCount,
                FileNameIncludeTimeStamp = _instance.FileNameStamp,
                DirectoryName = (_args.Outputfile == string.Empty ) ? _instance.OutputDirectory : Path.GetDirectoryName(_args.Outputfile),
                Filename = (_args.Outputfile == string.Empty ) ?
                    $@"{_instance.OutputFileName}{_instance.OutputFileExtension}"
                    :Path.GetFileName(_args.Outputfile)
            };

            Console.WriteLine(writer);

            if (!writer.IsValid())
            {
                Console.WriteLine("The specified output path is invalid ");
                return 4;
            }
           if (writer.ChunkSize > 0)
            {
                writer.WroteChunk = () => Console.Write(".");
            }

            writer.WriteFile(_instance.TableData());
            
            Console.WriteLine();
            Console.WriteLine($"Wrote {writer.FullFileName} with {writer.RecordCount} records");

            if ( (_instance.ZipFile) || (_args.Zipfile)) 
            {
                CompressFileZip(writer.FullFileName);
            }
            return 0;
        }

        private static void CompressFileGzip(string filename)
        {
            Console.WriteLine($"Starting file compression on {filename} at {DateTime.Now}");
            using (FileStream originalFileStream = new FileStream(@filename, FileMode.Open))
            {
                string zipFileName = $@"{Path.ChangeExtension(filename, "gzip")}";
                Console.WriteLine($"Saving zip file to {zipFileName}");
                using (FileStream compressedFileStream = File.Create(@zipFileName))
                {
                    using (GZipStream compressionStream =
                        new GZipStream(compressedFileStream, CompressionMode.Compress))
                    {
                        originalFileStream.CopyTo(compressionStream);
                    }
                }
            }

            Console.WriteLine($"File compression complete at {DateTime.Now}");
        }

        private static void CompressFileZip(string filename)
        {
            string zipFileName = $@"{Path.ChangeExtension(filename, "zip")}";
            if (File.Exists(zipFileName))
            {
                File.Delete(zipFileName);
            }
            Console.WriteLine($"Starting ZIP compression on {filename} at {DateTime.Now} to {zipFileName}");
            using (var zip = ZipFile.Open(zipFileName, ZipArchiveMode.Create))
            {
                zip.CreateEntryFromFile(filename,Path.GetFileName(filename));
            }
        }
    }
}