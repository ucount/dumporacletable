namespace DumpOracleTable
{
    public class Parameters
    {
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool ZipFile { get; set; }
        public string TableName { get; set; }
        public string OutputFolder { get; set; }
        public string ZipFolder { get; set; }
        
    }
}