using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace DumpOracleTable.CommandLineParameters
{
    public class CommandLineArguments
    {
        private const string Pattern = @"\/(?<argname>\w+):(?<argvalue>.+)";
        private readonly Regex _regex = new Regex(Pattern, RegexOptions.IgnoreCase|RegexOptions.Compiled);
        private readonly Dictionary<string, string> _args = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase); // ignore case on key
        private readonly List<string> _validParameters = new List<string>()
        {
            $"DumpOracleTable Version : {Assembly.GetExecutingAssembly().GetName().Version.ToString()}",
                            "\t /db:DATABASE [ DEFAULT to AppSettings.json ] , DATABASE in AA2DEV or AA2",
                            "\t /table:TABLENAME [ REQUIRED ], Table name as specified in C0541DATAHUB.Datahubcontrol",
                            "\t /populatetable:Y/N [Default to Y], Populate Oracle table from which to write data",
                            "\t /zip:Y/N [ DEFAULT to Oracle configured  ]. Zip output file",
                            "\t /out:outputlocation [ DEFAULT to DataHubControl value]. Override output location with full path ( Full path and filename )",
                            "\t /feedback:number [ DEFAULT to 10,000]. Feedback on number of rows written to file",
                            "\t /dateformat:dateformat [DEFAULT to DD-MON-YYYY]. The date format for all date columns",
                            "\t /rowlimit:number - Limit the number of rows to write"
        };

        public string Database => this["db"].ToUpper();
        public string Tablename => this["table"].ToUpper();
        public bool   Zipfile => this["zip"].ToUpper() != "N";

        public string Outputfile => this["out"];
        public int    FeedbackCount=> this["feedback"] == string.Empty ? 10000 : int.Parse(this["feedback"]);

        public int RowLimit => this["rowlimit"] == string.Empty ? 0 : int.Parse(this["rowlimit"]);

        public string DateFormat => this["DATEFORMAT"] == string.Empty ? "DD-MON-YYYY" : _args["DATEFORMAT"];
        
        public override string ToString()
        {
            return string.Join("\n", _validParameters.ToArray());
        }

        public bool ValidArguments => ContainsKey("TABLE") && ContainsKey("DB");
        
        public CommandLineArguments()
        {
            BuildArgDictionary();
        }
        
        private void BuildArgDictionary()
        {
            var args = Environment.GetCommandLineArgs();
            foreach (var match in args.Select(arg => _regex.Match(arg)).Where(m => m.Success))
            {
                try
                {
                    _args.Add(match.Groups["argname"].Value, match.Groups["argvalue"].Value);
                }
                // Ignore any duplicate args
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public string this[string key] => _args.ContainsKey(key) ? _args[key] : string.Empty;

        private bool ContainsKey(string key)
        {
            return _args.ContainsKey(key);
        }
        
        
    }
    
}