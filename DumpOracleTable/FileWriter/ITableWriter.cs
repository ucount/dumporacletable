using System.Collections;

namespace DumpOracleTable.FileWriter
{
    public interface ITableWriter
    {
        string Filename { get; set; }
        bool   HasHeader { get; set; }
        bool   HasFooter { get; set; }
        string Delimiter { get; set; }
        bool WriteFile(IEnumerable data);
    }
}