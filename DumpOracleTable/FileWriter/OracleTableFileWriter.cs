using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security;
using DumpOracleTable.FileWriter;

namespace DumpOracleTable
{
    public class OracleTableFileWriter:ITableWriter
    {
        public string DirectoryName { get; set; }
        public string Filename { get; set; }
        
        public string FileExtension { get; set; }
        public bool HasHeader { get; set; }
        public bool HasFooter { get; set; }
        public string Delimiter { get; set; }
        public int FileNumber { get; set; }

        public ulong RecordCount { get; set; }

        private string FileDateTime;
        
        public bool FileNameIncludeTimeStamp { get; set; }
        public string FullFileName
        {
            get 
        {
            var _filename = $@"{DirectoryName}\{Filename}"; 
            if (FileNameIncludeTimeStamp)
            {
                _filename = $@"{DirectoryName}\" +
                            $@"{Path.GetFileNameWithoutExtension(_filename)}" +
                            $"_{this.FileDateTime}" +
                            $"{Path.GetExtension(_filename)}";
            }

            return _filename.ToUpper();
        }
        }

        public int ChunkSize { get; set; }
        public Action WroteChunk;
        
        public int SequenceNo;
        
        public bool IsValid()
        {
            bool result = false;

            string path = this.FullFileName;
            if (String.IsNullOrWhiteSpace(path)) { return false; }

            try
            {
                var pathresult = Path.GetFullPath(path);
                result = Directory.Exists(Path.GetDirectoryName(pathresult));
            }
            catch (ArgumentException) { }
            catch (SecurityException) { }
            catch (NotSupportedException) { }
            catch (PathTooLongException) { }

            return result;

        }

        public OracleTableFileWriter()
        {
            this.FileDateTime = $"{DateTime.Now:yyyyMMddHHmmss}";
        }

        public override string ToString()
        {
            return $"\t Filename : {this.FullFileName}" +
                   $"\t DirectoryName : {this.DirectoryName}" +
                   $"\t Delimiter : {this.Delimiter}" +
                   $"\t SequenceNo : {this.SequenceNo}" +
                   $"\t FileNumber : {this.FileNumber}";
            
        }

        private string GetHeader()
        {
            if (!this.HasHeader)
            {
                return string.Empty;
            }
            return $"1{this.Delimiter}{DateTime.Now:yyyy-MM-dd}{this.Delimiter}{this.FileNumber}{this.Delimiter}{this.FullFileName.ToUpper()}{this.Delimiter}" +
                   $"{this.SequenceNo}{this.Delimiter}{DateTime.Now:yyyy-MM-dd hh:mm:ss}"
            ;
            
        }

        private string GetFooter()
        {
            if (!this.HasFooter)
            {
                return string.Empty;
            }

            return $"9{this.Delimiter}{DateTime.Now:yyyy-MM-dd}{this.Delimiter}{this.RecordCount}" +
                   $"{this.Delimiter}{this.FileNumber}{this.Delimiter}{ this.FullFileName.ToUpper()}" +
                   $"{this.Delimiter}{this.SequenceNo}{this.Delimiter}{DateTime.Now:yyyy-MM-dd hh:mm:ss}";
        }
        public bool WriteFile(IEnumerable data)
        {
            using (StreamWriter writer = File.CreateText(@FullFileName))
            {
                this.RecordCount = 0;
                CultureInfo info = CultureInfo.CurrentCulture;
                Console.WriteLine(info.DateTimeFormat.LongDatePattern);
                
                if (this.HasHeader)
                {
                    writer.WriteLine(this.GetHeader());
                }
                foreach (IDataRecord record in data)
                {
                    writer.Write($"2{this.Delimiter}");
                    for (int i = 0; i < record.FieldCount; i++)
                    {
                        if (record.GetFieldType(i) == typeof(DateTime))
                        {
                            writer.Write($"{record.GetDateTime(i):dd-MMM-yyyy}" +
                                         $"{(i < record.FieldCount - 1 ? this.Delimiter : Environment.NewLine)}");
                            
                        }
                        else
                        {

                            writer.Write($"{record.GetValue(i)}" +
                                         $"{(i < record.FieldCount - 1 ? this.Delimiter : Environment.NewLine)}");
                        }
                    }
                    if (++this.RecordCount % (ulong) ChunkSize == 0)
                    {
                        WroteChunk?.Invoke();
                    }
                }

                if (this.HasFooter)
                {
                    writer.WriteLine(this.GetFooter());
                }
                writer.Flush();
            }  

            return true;
        }
    }
}