namespace DumpOracleTable.Settings
{
    public class AppSettings
    {
        public string Username { get; set; }
        public string Database { get; set; }
    }
}