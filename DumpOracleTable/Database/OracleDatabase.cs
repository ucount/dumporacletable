using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DumpOracleTable.Database;
using Oracle.ManagedDataAccess.Client;

namespace DumpOracleTable
{
    public class OracleDatabase : IDatabase
    {

        private OracleConnectionStringBuilder _connectionString;
        public int SequenceNo { get; set; }
        public string OutputFileName { get; set; }
        public string OutputDirectory { get; set; }
        public string OutputFileExtension { get; set; }
        public int FileNumber { get; set; }
        public string TableName { get; set; }
        public bool ZipFile { get; set; }
        
        public string DateFormat { get; set; }
        public bool FileNameStamp { get; set; }
        
        public string Delimiter { get; set; }
        public int RowLimit { get; set; }
        
        public string DataSource
        {
            get => _connectionString.DataSource;
            set => _connectionString.DataSource = value;
        }

        public string UserName
        {
            get => _connectionString.UserID;
            set => _connectionString.UserID = value;
        }

        public string Password
        {
            get => _connectionString.Password;
            set => _connectionString.Password = value;
        }

        #region Singleton implementation 

        private static readonly Lazy<OracleDatabase> _instance = new Lazy<OracleDatabase>(() => new OracleDatabase());

        public static OracleDatabase Instance => _instance.Value;

        private OracleDatabase()
        {
            _connectionString = new OracleConnectionStringBuilder();
        }

        #endregion

        private OracleConnectionStringBuilder GetOracleConnectionString()
        {
            return _connectionString;
        }

        public bool Available()
        {
            using (OracleConnection _connection = new OracleConnection(GetOracleConnectionString().ConnectionString)) {
                try
                {
                    _connection.Open();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }
        }

        public void Dispose()
        {
        }

        public override string ToString()
        {
            string connectiondetails = $"Oracle Connection to {this.DataSource} as user {this.UserName}";
            if (string.IsNullOrEmpty(this.Password))
            {
                connectiondetails += $" - ERROR - No Password available";
            }

            return connectiondetails;

        }

        public bool GetDumpDetails(string tableName)
        {
            using (OracleConnection _connection = new OracleConnection(GetOracleConnectionString().ConnectionString))
            {
                try
                {
                    _connection.Open();
                    using (OracleCommand command = _connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText =
                            $"select * from c0541datahub.datahubcontrol where description=:description";

                        OracleParameter oracleParameter =
                            new OracleParameter("description", OracleDbType.Varchar2) {Value = tableName};
                        command.Parameters.Add(oracleParameter);
                        using (OracleDataReader reader = command.ExecuteReader())
                        {
                            command.FetchSize = reader.RowSize * 10000;
                            if (!reader.Read())
                            {
                                return false;
                            }

                            OutputFileName = reader.GetString(reader.GetOrdinal("OUTPUTFILENAME"));
                            this.OutputDirectory = reader.GetString(reader.GetOrdinal("OUTPUTDIRECTORY"));
                            this.OutputFileExtension = reader.GetString(reader.GetOrdinal("OUTPUTFILEEXTENSION"));
                            this.ZipFile = (reader.GetString(reader.GetOrdinal("ZIPFILE")).ToUpper() == "Y");
                            this.SequenceNo = reader.GetInt32(reader.GetOrdinal("SEQUENCENO"));
                            this.TableName = reader.GetString(reader.GetOrdinal("TABLENAME"));
                            this.FileNumber = reader.GetInt32(reader.GetOrdinal("DATAHUBCONTROLID"));
                            this.Delimiter = reader.GetString(reader.GetOrdinal("DELIMITER"));
                            this.FileNameStamp = (reader.GetString(reader.GetOrdinal("FILENAMETIMESTAMP")).ToUpper() == "Y");
                        }
                    }

                    return true;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }


        public IEnumerable<IDataRecord> TableData()
        {
            
            
            using (OracleConnection _connection = new OracleConnection(GetOracleConnectionString().ConnectionString))
            {
                _connection.Open();
                
                OracleGlobalization info = _connection.GetSessionInfo();
                //info.DateFormat = string.IsNullOrEmpty(this.DateFormat) ? "DD-MON-YYYY" : this.DateFormat;
                info.DateFormat = "DD-MON-YYYY";
                _connection.SetSessionInfo(info);

                using (OracleCommand _command = _connection.CreateCommand())
                {
                    _command.CommandType = CommandType.Text;
                    _command.FetchSize = 100000;
                    StringBuilder sql = new StringBuilder(@"select * from ");
                    sql.Append(this.TableName);
                    if (RowLimit != 0)
                    {
                        sql.Append($" where rownum < {this.RowLimit}");
                    }
                    _command.CommandText = sql.ToString();
                   using (OracleDataReader _reader = _command.ExecuteReader())
                   {
                       while (_reader.Read())
                       {
                           yield return _reader;
                       }
                   }
                }
            }
        }
        
    }
}