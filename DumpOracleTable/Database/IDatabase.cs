using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.CompilerServices;

namespace DumpOracleTable.Database
{
    public interface IDatabase:IDisposable
    {
        int    SequenceNo { get; set; }
        string OutputFileName { get; set; }
        string OutputDirectory { get; set; }
        string OutputFileExtension { get; set; }
        string TableName { get; set; }
        bool   ZipFile { get; set; }
        
        string DataSource { get; set; }
        
        string UserName { get; set; }
        
        string Password { get; set; }

        bool Available();

        bool GetDumpDetails(string tableName);
    }
}