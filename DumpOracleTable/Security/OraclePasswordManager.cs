using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Elskom.Generic.Libs;

//using Org.BouncyCastle.Utilities.Encoders;

namespace DumpOracleTable.Security
{
	public enum OracleServer
	{
		Production,
		Test,
		Development
	}

	public class OraclePasswordManager
	{
		#region Private Constants
		private const string PasswordFilename = @"C:\inetpub\oracle\oraclepassword.enc";
        //private const string PasswordFilename = @"c:\inetpub\oracle\OraclePassword.enc";
        private const string PasswordKey = "h0td0g13";
		#endregion

		#region Private Fields
		/// <summary>
		/// Encoding component
		/// </summary>
		private UTF32Encoding encoding = new UTF32Encoding();

		private List<String> passwords = new List<string>();
		/// <summary>
		/// List of Decrypted Passwords
		/// </summary>
		public List<String> Passwords
		{
			get { return this.passwords; }
			set { this.passwords = value; }
		}
		/// <summary>
		/// Encryptor
		/// </summary>
		private BlowFish encryptor;

		#endregion

		#region Private Methods
		#endregion

		#region Instance Management
		/// <summary>
		/// Default Constructor
		/// </summary>
		private OraclePasswordManager()
		{
			this.encryptor = new BlowFish(this.encoding.GetBytes(PasswordKey));
			ReadPasswords();
		}

#if DEBUG
		~OraclePasswordManager()
		{
			WriteLog("Free");
		}

		private void WriteLog(string text)
		{
            FileStream file = new FileStream(@"c:\inetpub\oracle\OraclePasswordManager.log", FileMode.Append);
			StreamWriter writer = new StreamWriter(file);
			try
			{
				writer.WriteLine("OraclePasswordManager: " + text);
			}
			finally
			{
				writer.Close();
				file.Close();
			}
		}
#endif
		#endregion

		#region Public Methods
		/// <summary>
		/// Read the Password for the spcecified Oracle Server
		/// </summary>
		/// <param name="server">The enumerated valued of the specified Oracle Server</param>
		/// <returns>The password for the Oracle Server</returns>
		public string GetOraclePassword(OracleServer server)
		{
			return this.Passwords[Convert.ToInt32(server)];
		}

		/// <summary>
		/// Read the Password for the specified Oracle Server
		/// </summary>
		/// <param name="serverName">The name of the specified Oracle Server</param>
		/// <returns>The password for the Oracle Server</returns>
		public string GetOraclePassword(string serverName)
		{
			switch (serverName.ToUpper())
			{
				case "AA2":
					return GetOraclePassword(OracleServer.Production);
				case "AA2TEST":
					return GetOraclePassword(OracleServer.Test);
				case "AA2DEV":
					return GetOraclePassword(OracleServer.Development);
				default:
					return String.Empty;
			}
		}

		/// <summary>
		/// Read Oracle passwords from the file
		/// </summary>
		public void ReadPasswords()
		{
			//Open the password file
			FileStream stream = new FileStream(PasswordFilename, FileMode.Open,FileAccess.Read);
			StreamReader reader = new StreamReader(stream);
			try
			{
				// Read the encrypted passwords
				this.passwords.Clear();
				while (!reader.EndOfStream)
				{
					this.passwords.Add(this.encryptor.DecryptECB(reader.ReadLine()));
				}
			}
			catch
				(System.Security.SecurityException)
			{
				Console.WriteLine($"Unable to access password file {PasswordFilename}");
			}
					
			finally
			{
				reader.Close();
				stream.Close();
			}
		}

		/// <summary>
		/// Write the passwords to the Stream
		/// </summary>
		public void WritePasswords()
		{
			// Open/Create the file
			FileStream stream = new FileStream(PasswordFilename, FileMode.Create);
			StreamWriter writer = new StreamWriter(stream);

			// Write the encrypted passwords to the File
			try
			{
				foreach (string password in this.passwords)
				{
					string encryptedPassword = this.encryptor.EncryptECB(password);
					writer.WriteLine(encryptedPassword);
				}
				
			}
			finally
			{
				// Close Writer and Stream
				writer.Close();
				stream.Close();
			}
		}
		#endregion

		#region Public Static Properties
		private static OraclePasswordManager instance = null;
		/// <summary>
		/// Instance of the Manager
		/// </summary>
		public static OraclePasswordManager Instance 
		{
			get
			{
				if (instance == null)
				{
					instance = new OraclePasswordManager();
				}

				return instance;
			}
		}
		#endregion

	}
}
