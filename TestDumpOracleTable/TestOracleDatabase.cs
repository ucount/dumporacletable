using DumpOracleTable;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class TestOracleDatabase
    {
        [Test]
        public void TestSingletonTrue()
        {
            var db1 = OracleDatabase.Instance;
            var db2 = OracleDatabase.Instance;
            Assert.That(db1, Is.EqualTo(db2));
        }
        
        [Test]
        public void TestOracleConnectionPass()
        {
            OracleDatabase.Instance.UserName = "denvers";
            OracleDatabase.Instance.Password = "angie#15";
            OracleDatabase.Instance.DataSource = "192.168.196.211/aadev.awards.co.za";
            Assert.That(OracleDatabase.Instance.Available,Is.True);
        }

        [Test]
        public void TestGetDumpDetailsReturnFalse()
        {
            var _instance = OracleDatabase.Instance;
            _instance.UserName = "denvers";
            _instance.Password = "angie#15";
            _instance.DataSource = "192.168.196.211/aadev.awards.co.za";
            bool _result = _instance.GetDumpDetails("TEST");
            
            Assert.That(_result, Is.False);
        }

        [Test]
        public void TestGetDumpDetails_Pass_AccPosTranGet()
        {
            var _instance = OracleDatabase.Instance;
            _instance.UserName = "denvers";
                
            _instance.Password = "angie#15";
            _instance.DataSource = "192.168.196.211/aadev.awards.co.za";
            
            bool _result = _instance.GetDumpDetails("ACCOUNTPOSTRANSACTION");
            Assert.That(_result,Is.True);
            Assert.That(_instance.TableName,Is.EqualTo("C0541DATAHUB.ACCOUNTPOSTRANSACTION"));
        }

        [Test]
        public void TestDumpTable_Pass()
        {
            
        }
    }
}